import React from 'react'

export default function Button(props) {

    const {
        color = "blue",
        fullWidth = false,
        size = "normal"
    } = props

    const colors = {
        red: "bg-red-500 hover:bg-red-400 text-white",
        blue: "bg-blue-600 hover:bg-blue-500 text-white",
        green: "bg-green-600 hover:bg-green-500 text-white"
    }

    const sizes = {
        small: "py-0.5",
        normal: "py-1",
        big: "py-2"
    }

    const propsBtn = { ...props }
    delete propsBtn.color;
    delete propsBtn.fullWidth;
    delete propsBtn.size;

    return (
        <button
            className={` 
                rounded-full
                ${colors[color]}
                ${fullWidth ? "w-full" : "px-5"}
                ${sizes[size]}
            `}
            {...propsBtn}
        >
            {props.children}
        </button>
    )
}
