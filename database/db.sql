CREATE DATABASE workers;

use workers;

CREATE TABLE
    trabajadores (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        tipo TEXT NOT NULL,
        nombre TEXT NOT NULL,
        sueldo INT NULL,
        entregas TEXT NOT NULL,
        horas INT NOT NULL
    );

describe trabajadores;

