import axios from "axios"

const endpoint = "/api/workers/"

function getTrabajadores(orderby = null) {
    return new Promise( async ( resolve, reject ) => {
        try {
            const { data } = await axios.get( endpoint, {params: { orderby }} );
            resolve( data )
        } catch( error ) {
            reject(new Error( error ))
        }
    })
}

function getTrabajador(id) {
    return new Promise( async ( resolve, reject ) => {
        try {
            const { data } = await axios.get(endpoint + id);
            resolve( data );
        } catch( error ) {
            reject(new Error( error ));
        }
    })
}

function createTrabajador(payload) {
    return new Promise( async (resolve, reject) => {
        try {
            const { data } = await axios.post( endpoint, payload )
            resolve( data )
        } catch( error ) {
            reject(new Error(error))
        }
    })
}

function updateTrabajador(id, payload) {
    return new Promise( async ( resolve, reject ) => {
        try {
            const { data } = await axios.put(endpoint + id, payload);
            resolve(data);
        } catch( error ) {
            reject( new Error( error ));
        }
    })
}

function deleteTrabajador(id) {
    return new Promise( async (resolve, reject) => {
        try {
            const { data } = await axios.delete(endpoint + id);
            resolve( data );
        } catch(error) {
            reject(new Error( error ));
        }
    })
}

const functions = {
    getTrabajador, getTrabajadores, createTrabajador, updateTrabajador, deleteTrabajador
}

export default functions