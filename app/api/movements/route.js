import { NextResponse } from "next/server";
import { pool } from '@/configDB/db';

const tableName = 'movimientos'

export async function POST(request) {
  // TODO: Cambiar los datos de los movimientos 
  // TODO: const { tipo, nombre, entregas, horas } = await request.json();
  const sueldo = null;
  try {
    await pool.query(`INSERT INTO ${tableName} SET ?`, { tipo, nombre, sueldo, entregas, horas });
    return NextResponse.json({ message: "Trabajador guardado" }, { status: 201 });
  }
  catch (error) {
    return NextResponse.json({ message: error.message }, { status: 400 });
  }
}

export async function GET(req) {
  const orderby = req.nextUrl.searchParams.get('orderby');

  let query = "";
  if(orderby) {
    query += `ORDER BY ${orderby} `
  }

  const [rows] = await pool.query(`SELECT * FROM ${tableName} ${query};`)
  return NextResponse.json([rows][0]);
}