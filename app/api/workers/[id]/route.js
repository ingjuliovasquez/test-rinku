import { NextResponse } from "next/server";
import { pool } from '@/configDB/db';

const tableName = 'trabajadores'

export async function PUT(request, { params }) {
  const { id } = params;
  const { tipo, nombre, entregas, horas } = await request.json();
  const sueldo = null;
  try {
    await pool.query(`UPDATE ${tableName} SET ? WHERE id = ?`, [{ tipo, nombre, sueldo, entregas, horas }, id])
    return NextResponse.json({ message: "Trabajador actalizado" }, { status: 200 });
  } catch(error) {
    return NextResponse.json({ message: error.message }, { status: 400 });
  }
}

export async function GET(_, { params }) {
  const { id } = params;
  const [worker] = await pool.query(`SELECT * FROM ${tableName} WHERE id = ?`, [id])
  return NextResponse.json([worker][0][0], { status: 200 });
}

export async function DELETE(_, { params }) {
  const { id } = params;
  try {
    await pool.query(`DELETE FROM ${tableName} WHERE id = ?`, [id])
    return NextResponse.json({ message: "Trabajador eliminado" }, { status: 200 });
  }
  catch (error) {
    return NextResponse.json({ message: error.message }, { status: 400 });
  }


}


