"use client"
import { useEffect, useState } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material'
import db from '@/database/controller'
import columns from '@/constants/workersColumns';
import Button from '@/components/Button';

import calcularSaldo from '@/functions/calcularSaldo';


const initFormData = {
  entregas: '',
  horas: '',
  id: null,
  nombre: '',
  tipo: '',
}

export default function Home() {

  //* List
  const [workers, setWorkers] = useState([])
  const [selected, setSelected] = useState([])

  //* Dialogs
  const [dialogForm, setDialogForm] = useState(false)
  const [formData, setFormData] = useState(initFormData)

  const [deleteDialog, setDeleteDialog] = useState(false)

  const [saldosDialog, setSaldosDialog] = useState(false)
  const [saldos, setSaldos] = useState(null)


  //* Database functions

  async function getData() {
    try {
      const data = await db.getTrabajadores()
      setWorkers(data)
    } catch (e) {
      alert(e)
    }
  }

  async function saveData() {
    const payload = { ...formData }
    delete payload.id
    try {
      if (!formData.id) {
        const { message } = await db.createTrabajador(payload)
        console.log(message)
      } else {
        const { message } = await db.updateTrabajador(formData.id, payload)
        console.log(message)
      }
      getData()
      closeDialog()
    } catch (e) {
      alert(e)
    }
  }

  async function deleteWorker() {
    try {
      await db.deleteTrabajador(selected[0])
      getData()
      closeDeleteDialog()
    } catch (e) {
      alert(e)
    }
  }

  useEffect(() => { getData() }, [])



  //* Add / Edit Dialog functions

  const openDialog = () => {
    setDialogForm(true)
  }
  const closeDialog = () => {
    setDialogForm(false)
    setFormData(initFormData)
  }

  const submit = e => {
    e.preventDefault()
    saveData()
  }

  const editSelected = () => {
    const edited = workers.find(worker => worker.id === selected[0])
    if (edited) {
      delete edited.sueldo
      setFormData(edited)
      openDialog()
    }
  }

  const changeValue = (e, key) => setFormData({ ...formData, [key]: e.target.value })



  //* Delete dialog functions

  const openDeleteDialog = () => setDeleteDialog(true);
  const closeDeleteDialog = () => setDeleteDialog(false);
  const confirmDelete = () => { deleteWorker() }





  //* Saldos
  const openSaldosDialog = (s) => {
    setSaldos(s)
    setSaldosDialog(true)
  }

  const closeSaldosDialog = () => {
    setSaldos(null)
    setSaldosDialog(false)
  }



  const calculate = () => {
    const _worker = workers.find(worker => worker.id === selected[0])
    try {
      const _saldos = calcularSaldo(_worker)
      openSaldosDialog(_saldos)
    } catch (e) {
      alert(e)
    }

  }





  return <main className="w-full">
    <div className="container mx-auto" >
      <h2 className='text-xl' >Trabajadores</h2>
      <div className="flex items-center w-full justify-between my-4">
        <Button size="small" onClick={openDialog} >Agregar</Button>
        <div className="flex items-center gap-2">
          {
            selected.length > 0 && <>
              <Button size="small" color="green" onClick={calculate} >Calcular saldo</Button>
              <Button size="small" onClick={editSelected} >Editar</Button>
              <Button size="small" color="red" onClick={openDeleteDialog} >Eliminar</Button>
            </>

          }
        </div>
      </div>
      <DataGrid
        rows={workers}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: {
              pageSize: 10,
            },
          },
        }}
        pageSizeOptions={[10]}
        onRowSelectionModelChange={(newSelected) => {
          if (selected[0] === newSelected[0]) {
            setSelected([])
          } else {
            setSelected(newSelected);
          }
        }}
        rowSelectionModel={selected}
      />
    </div>

    <Dialog open={dialogForm} onClose={closeDialog} fullWidth maxWidth='xs' >
      <DialogTitle sx={{ background: "#1565c0", py: 1, color: "white" }}  >
        {formData.id ? "Editar" : "Agregar"}
      </DialogTitle>
      <form onSubmit={submit} >
        <DialogContent>
          <div className="flex flex-col gap-3 py-3">
            <TextField fullWidth size="small" label="Nombre" required
              value={formData.nombre} onChange={e => changeValue(e, 'nombre')} />
            <FormControl fullWidth size="small" required >
              <InputLabel>Tipo</InputLabel>
              <Select fullWidth size="small" label="Tipo"
                value={formData.tipo} onChange={e => changeValue(e, 'tipo')} >
                <MenuItem value='' disabled> <i>Selecciona</i> </MenuItem>
                <MenuItem value={0}>Chofer</MenuItem>
                <MenuItem value={1}>Cargador</MenuItem>
                <MenuItem value={2}>Auxiliar</MenuItem>
              </Select>
            </FormControl>
            <TextField fullWidth size="small" label="Entregas" type="number" required
              value={formData.entregas} onChange={e => changeValue(e, 'entregas')} />
            <TextField fullWidth size="small" label="Horas" type="number" required
              value={formData.horas} onChange={e => changeValue(e, 'horas')} />
          </div>
        </DialogContent>
        <DialogActions>
          <Button type="submit" size="small" >Guardar</Button>
          <Button color="red" size="small" onClick={closeDialog} type="button" >Cancelar</Button>
        </DialogActions>
      </form>
    </Dialog>


    <Dialog open={deleteDialog} onClose={closeDeleteDialog} fullWidth maxWidth='xs' >
      <DialogTitle sx={{ background: "#1565c0", py: 1, color: "white" }}  >
        Confirma borrar
      </DialogTitle>
      <DialogContent>
        <h5 className='mt-5' >¿Seguro que desea borrar este trabajador?</h5>
        <h5>Esta acción no se puede deshacer</h5>
      </DialogContent>
      <DialogActions>
        <Button color="red" size="small" onClick={confirmDelete} >Borrar</Button>
        <Button size="small" onClick={closeDeleteDialog} >Cancelar</Button>
      </DialogActions>
    </Dialog>

    <Dialog open={saldosDialog} onClose={closeSaldosDialog} fullWidth maxWidth='xs' >
      <DialogTitle sx={{ background: "#1565c0", py: 1, color: "white" }}  >
        Saldo mensual
      </DialogTitle>
      {saldos &&
        <DialogContent>
          <h2 className='mt-5 font-bold' >Saldo para {saldos.worker.nombre}</h2>
          <hr></hr>
          <div className="flex w-full justify-between">
            <h5>Saldo base:</h5>
            <h5> {saldos.base} </h5>
          </div>
          <div className="flex w-full justify-between">
            <h5>Saldo por bono:</h5>
            <h5> {saldos.bono} </h5>
          </div>
          <div className="flex w-full justify-between">
            <h5>Saldo por entregas:</h5>
            <h5> {saldos.entregas} </h5>
          </div>
          <div className="flex w-full justify-between">
            <h5>Saldo bruto total:</h5>
            <h5> {saldos.bruto} </h5>
          </div>
          <div className="flex w-full justify-between">
            <h5>ISR:</h5>
            <h5 className='text-red-600' > - {saldos.ISR} </h5>
          </div>
          <hr/>
          <div className="flex w-full justify-between">
            <h5>Saldo neto total:</h5>
            <h5 className='text-green-600 font-bold' > {saldos.neto} </h5>
          </div>
          <div className="flex w-full justify-between">
            <h5>Vales de despensa:</h5>
            <h5> {saldos.vales} </h5>
          </div>
        </DialogContent>
      }
      <DialogActions>
        <Button size="small" onClick={closeSaldosDialog} >Cerrar</Button>
      </DialogActions>
    </Dialog>

  </main>
}
