const tipos = [
    "Chofer",
    "Cargador",
    "Auxiliar"
]

const workersColumns = [
    { field: 'id', headerName: 'ID', width: 90 },
    {
        field: 'tipo',
        headerName: 'Tipo',
        width: 150,
        valueGetter: (params) => `${tipos[params.row.tipo]}`,
    },
    {
        field: 'nombre',
        headerName: 'Nombre',
        width: 150,
    },
    {
        field: 'entregas',
        headerName: 'Entregas',
        type: 'number',
        description: 'Entregas realizadas',
        width: 110,
    },
    {
        field: 'horas',
        headerName: 'Horas',
        type: 'number',
        description: 'Horas trabajadas',
        width: 160,
    },
];

export default workersColumns;