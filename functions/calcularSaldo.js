/* 
* Sueldos 
*
*
* Sueldo base por hora: 30 (para todos)
* Entregas: 5 (para todos)
*
*
* chofer
* bono: 10
* 
* cargador
* bono: 5
* 
* auxiliar 
* bono: 0
*
* Saldo bruto: (sueldo base x horas trabajadas) + (bono x horas trabajadas) + (5 x entregas)
* ISR: Saldo bruto > 10000 ? 12% : 9%
* Saldo neto: Saldo bruto - ISR
* Vales de despensa: 4% de saldo bruto

*/
export default function calcularSaldo(worker) {

    const sueldos = {
        base: 30,
        entrega: 5,
        bono: [10, 5, 0]
    }
    console.log(worker)

    const horas = sueldos.base * worker.horas;
    const bono = sueldos.bono[worker.tipo] * worker.horas;
    const entregas = sueldos.entrega * worker.entregas;
    const bruto = horas + bono + entregas;
    const ISR = (bruto > 10000 ? bruto * 0.12 : bruto * 0.09);
    const neto = bruto - ISR;
    const vales = bruto * 0.04

    return { worker, horas, bono, entregas, bruto, ISR, neto, vales }

}